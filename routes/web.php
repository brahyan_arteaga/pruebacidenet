<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdministrarController@index');
Route::get('/agregar', 'AgregarController@index');
Route::get('/editar/{id}', 'EditarController@index');
Route::get('/quitar', 'BuscarController@quitar');

Route::post('/buscar', 'BuscarController@index');
Route::post('/eliminar', 'EliminarController@index');
Route::post('/registrar', 'AgregarController@registrar');
Route::post('/actualizar', 'EditarController@actualizar');

Route::get('/welcome', function () {
    return view('welcome');
});





