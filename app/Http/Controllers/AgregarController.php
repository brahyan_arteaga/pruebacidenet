<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use Illuminate\Support\Facades\Log;
/**
 * Controlador para la vista agregar.blade.php
 */
class AgregarController extends Controller
{
    
    /**
     * metodo que inicializa y retorna la vista agregar.blade.php
     */
    public function index()
    {
        
        return view('layouts.agregar');
    }

    /**
     * Metodo para realizar el registro de la persona a la base de datos
     */
    public function registrar(Request $request){
        $personas = \DB::table('persona')->get(); // consulta a la base de datos
        $objetoPersona = new Persona; // se inicializa el modelo

        //--------------Se capturan los datos de los input en la vista--------------//
        $objetoPersona->primerApellido = $request->input('apellido1');
        $objetoPersona->segundoApellido = $request->input('apellido2');
        $objetoPersona->primerNombre = $request->input('nombre');
        $objetoPersona->otrosNombres = $request->input('otros');
        $objetoPersona->tipoIdentificacion = $_POST['tipoIdentificacion'];
        $objetoPersona->numeroIdentificacion = $request->input('identificacion');
        $objetoPersona->pais = $_POST['pais'];
        $objetoPersona->correo;
        $objetoPersona->fechaIngreso = $_POST['fecha_ingresada'];
        $objetoPersona->area = $_POST['area'];

        $consultaPersona = Persona::where('persona.numeroIdentificacion','=',$request->input('identificacion'))->first();

        //-----Se recorre sobre la consulta para obtener el ultimo id-------------------//
        $idAux = 0;
        foreach ($personas as $persona) {
            $idAux = $persona->id;
        }

        //---------------------se incrementa el id para asignarlo a la variable correo electronico-------//
        $idAux += 1;
        $apellidoSinEspacio = str_replace(' ', '', $request->input('apellido1'));//quitar espacios por si es compuesto el apellido
        $objetoPersona->correo = $request->input('nombre').".".$apellidoSinEspacio.".".$idAux."@cidenet.com.us";
        
        $fecha = date("Y-m-d");// se obtiene la fecha actual
        $fechaActual = strtotime($fecha);// se calcula los segundos de la fecha actual
        $fechaIngresada = strtotime($_POST['fecha_ingresada']);//se calcula los segundos de la fecha ingresada

        $mesMenor = strtotime($fecha."- 1 month");//se le resta un mes a la fecha actual

        //Se realiza la validacion de las fechas
        if ($fechaIngresada > $fechaActual || $fechaIngresada < $mesMenor) {
            Log::debug('Fecha inválida en el proceso de registro');//Guarda el log con el mensaje indicado
            return redirect('/agregar')->with('status', 'Fecha invalida');//Se redirecciona a la pagina agregar
        }else{
            /**
            * Try catch para manejar las posibles excepciones a la hora de realizar el registro
            */
            try {
                //---- Se realiza el registro a la base de datos--------//
                $objetoPersona->save();
                $objetoPersona = persona::all();
                return redirect('/')->with('status', 'Persona agregada');
            } catch (\Throwable $th) {
                Log::debug('La persona ya existe -> proceso de registro' . $th->getMessage());//Guarda el log con el mensaje indicado
                return redirect('/agregar')->with('status', 'El usuario ya existe');//Se redirecciona a la pagina agregar
            }
        }
        
    }
}
