<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use Illuminate\Support\Facades\Log;
/**
 * Controlador para eliminar registros en la vista administrar.blade.php
 */
class EliminarController extends Controller
{
    //Función para eliminar los registros en la base de datos y actualizar la vista 
    public function index(Request $request)
   {
    $id = $_POST['id']; // se captura el id de la persona que se desea eliminar

    /**
     * Try catch para manejar las posibles excepciones a la hora de realizar el registro
     */
    try {
        $personas = Persona::where('id','=',$id)->first();// se realiza la consulta para eliminar ese registro
        $personas->delete(); // se elimina el registro de la base de datos
        return redirect('/')->with('statusDelete', 'Persona eliminada'); //Se actualiza la vista
    } catch (\Throwable $th) {
        Log::debug('No se pudo eliminar la persona' . $th->getMessage());//Guarda el log con el mensaje indicado
        return redirect('/')->with('statusDelete', 'No se pudo eliminar la persona'); //Se actualiza la vista
    }

    
    
    
    
   }
}
