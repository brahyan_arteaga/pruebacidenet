<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Controlador para la vista administrar.blade.php
 */
class AdministrarController extends Controller
{
    
    /**
     * Metodo que inicializa la vista con los registros de la base de datos
     */
    public function index()
    {
        $bandera = false; //bandera utilizada para ocultar o visualizar elementos en la vista html 
        $personas = \DB::table('persona')->get(); // consulta a la base de datos
        
        return view('layouts.administrar', compact('personas','bandera'));//Se redirecciona a la pagina principal
    }
}
