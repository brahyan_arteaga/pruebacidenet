<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use Illuminate\Support\Facades\Log;

/**
 * Controlador para la vista editar.blade.php
 */
class EditarController extends Controller
{
    /**
     * Metodo que inicializa la vista recibiendo el id para realizar
     * la consulta a la base de datos y mostrar la vista con los 
     * elementos de la consulta
     */
   public function index($id)
   {
    //Consulta a la base de datos con el parametro id
    $personas = \DB::table('persona')
                ->where('persona.id','=',$id)
                ->get();
    return view('layouts.editar', compact('personas'));
   }

   /**
    * Funcion para actualizar el registro en la base de datos
    */
   public function actualizar(Request $request){

    $id = $_POST['id']; //se captura el id 

    $personas = Persona::where('persona.id','=',$id)->first(); //consulta a la base de datos segun el id

    //verificar si existen registros
    if (!empty($personas)) {

            //--------------Se capturan los datos de los input en la vista--------------//
            $personas->primerApellido = $request->input('apellido1');
            $personas->segundoApellido = $request->input('apellido2');
            $personas->primerNombre = $request->input('nombre');
            $personas->otrosNombres = $request->input('otros');
            $personas->tipoIdentificacion = $_POST['tipoIdentificacion'];
            $personas->numeroIdentificacion = $request->input('identificacion');
            $personas->pais = $_POST['pais'];
            $personas->correo;
            $personas->fechaModificacion = $_POST['fecha_ingresada'];
            $personas->area = $_POST['area'];

            $apellidoSinEspacio = str_replace(' ', '', $request->input('apellido1'));//quitar espacios por si es compuesto el apellido
            $personas->correo = $request->input('nombre').".".$apellidoSinEspacio.".".$id."@cidenet.com.us";
            
            
            $fecha = date("Y-m-d");// se obtiene la fecha actual
            $fechaActual = strtotime($fecha);// se calcula los segundos de la fecha actual
            $fechaIngresada = strtotime($_POST['fecha_ingresada']);//se calcula los segundos de la fecha ingresada
            $mesMenor = strtotime($fecha."- 1 month");//se le resta un mes a la fecha actual

            //Se realiza la validacion de las fechas
            if ($fechaIngresada > $fechaActual || $fechaIngresada < $mesMenor) {
                Log::debug('Fecha inválida en el proceso de edición');//Guarda el log con el mensaje indicado
                return redirect('/editar/'.$id)->with('status', 'Fecha inválida');//Se redirecciona a la pagina editar
            }else{
                /**
                 * Try catch para manejar las posibles excepciones a la hora de realizar el registro
                 */
                try {
                    //---- Se realiza el registro a la base de datos--------//
                    $personas->save();
                    $personas = persona::all();
                    return redirect('/')->with('status', 'Persona actualizada');
                } catch (\Throwable $th) {
                    Log::debug('El documento que intenta actualizar, ya está registrado -> proceso de edición' . $th->getMessage());//Guarda el log con el mensaje indicado
                    return redirect('/editar/'.$id)->with('status', 'El documento que intenta actualizar, ya está registrado');//Se redirecciona a la pagina editar
                }
                
                
            }
        
    }else{
        return redirect('/')->with('status', 'No existe persona para editar');
    }
    
   }
}
