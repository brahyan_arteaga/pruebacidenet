<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
/**
 * Controlador para la tarea de realizar busqueda en la vista administrar.blade.php
 */
class BuscarController extends Controller
{
   /**
    * Realiza la busqueda a la base de datos segun el termino ingresado, 
    * retornando la vista con los elementos de la consulta.
    */
   public function index(Request $request)
   {
    $bandera = true; //bandera utilizada para ocultar o visualizar elementos en la vista html 
    $termino = $request->input('buscar'); // termino a buscar

    //Consulta a la base de datos comparando con el termino ingresado
    $personas = \DB::table('persona')
                ->where('persona.primerNombre','=',$termino)
                ->orWhere('persona.primerApellido','=',$termino)
                ->orWhere('persona.segundoApellido','=',$termino)
                ->orWhere('persona.otrosNombres','=',$termino)
                ->orWhere('persona.tipoIdentificacion','=',$termino)
                ->orWhere('persona.numeroIdentificacion','=',$termino)
                ->orWhere('persona.pais','=',$termino)
                ->orWhere('persona.correo','=',$termino)
                ->get();
    
    return view('layouts.administrar', compact('personas','bandera'));//Se redirecciona a la pagina principal
   }

   /**
    * Metodo para quitar los filtros de busqueda
    */
   public function quitar()
   {
    return redirect('/');//Se redirecciona a la pagina principal
   }
}
