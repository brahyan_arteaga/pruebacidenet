@extends('welcome')
@section('añadir')
    @if (session('status'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row m-5">
        <div class="col">
            <h1 class="mt-3 text-center animated fadeIn fast">AGREGAR PERSONA</h1>
            <hr>
            <div class="card bg-light">
                <div class="card-header">
                    <h3>Formulario</h3>
                </div>
                <div class="card-body">
                    <form class="was-validated" action="{{ url('/registrar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Primer Apellido</label>
                            <input name="apellido1" type="text" class="form-control" id="apellido1"
                                aria-describedby="nameHelp" maxlength="20" pattern="[^a-zñÑÁÉÍÓÚ]+" required>
                            <small id="nameHelp" class="form-text text-muted">Ingrese su primer apellido</small>
                        </div>
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                            <input name="apellido2" type="text" class="form-control" id="apellido2"
                                aria-describedby="nameHelp" maxlength="20" pattern="[^a-zñÑÁÉÍÓÚ]+" required>
                            <small id="nameHelp" class="form-text text-muted">Ingrese su segundo apellido</small>
                        </div>
                        <div class="form-group">
                            <label>Primer Nombre</label>
                            <input name="nombre" type="text" class="form-control" id="nombre" aria-describedby="nameHelp"
                                maxlength="20" pattern="[^a-zñÑÁÉÍÓÚ]+" required>
                            <small id="nameHelp" class="form-text text-muted">Ingrese su primer nombre</small>
                        </div>
                        <div class="form-group">
                            <label>Otros nombres (Opcional)</label>
                            <input name="otros" type="text" class="form-control" id="otros" aria-describedby="nameHelp"
                                maxlength="50" pattern="[^a-zñÑÁÉÍÓÚ]+">
                            <small id="nameHelp" class="form-text text-muted">Ingrese sus otros nombres</small>
                        </div>
                        <div class="form-group">
                            <label>País del empleo</label>
                            <select class="form-select" aria-label="Default select example" name="pais" required>
                                <option selected disabled value="">País</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Estados Unidos">Estados Unidos</option>
                            </select>
                            <small id="nameHelp" class="form-text text-muted">Ingrese el país del empleo</small>
                        </div>
                        <div class="form-group">
                            <label>Tipo de Identificación</label>
                            <select class="form-select" aria-label="Default select example" name="tipoIdentificacion"
                                required>
                                <option selected disabled value="">Tipo de identidicación</option>
                                <option value="Cédula de Ciudadanía">Cédula de Ciudadanía</option>
                                <option value="Cédula de Extranjería">Cédula de Extranjería</option>
                                <option value="Pasaporte">Pasaporte</option>
                                <option value="Permiso Especial">Permiso Especial</option>
                            </select>
                            <small id="nameHelp" class="form-text text-muted">Ingrese el tipo de Identificación</small>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input name="identificacion" type="text" class="form-control" id="identificacion"
                                aria-describedby="nameHelp" maxlength="20" pattern="[a-zA-z0-9-]+" required>
                            <small id="nameHelp" class="form-text text-muted">Ingrese su número de identidicación</small>
                        </div>
                        <div class="form-group">
                            <label name="fecha" for="fecha_ingresada">Fecha:</label>
                            <input type="date" class="form-control" name="fecha_ingresada" id="fecha_ingresada"
                                aria-describedby="fechaHelp" required>
                            <small id="fechaHelp" class="form-text text-muted">Seleccionar la fecha</small>
                            <br>
                        </div>
                        <div class="form-group">
                            <label>Área</label>
                            <select class="form-select" aria-label="Default select example" name="area" required>
                                <option selected disabled value="">Seleccione área</option>
                                <option value="Administración">Administración</option>
                                <option value="Financiera">Financiera</option>
                                <option value="Compras">Compras</option>
                                <option value="Infraestructura">Infraestructura</option>
                                <option value="Operación">Operación</option>
                                <option value="Talento Humano">Talento Humano</option>
                                <option value="Servicios Varios">Servicios Varios</option>

                            </select>
                            <small id="nameHelp" class="form-text text-muted">Ingrese el área para la cual fue contratado el
                                empleado</small>
                        </div>
                        <div style="text-align: end;">
                            <div class="btn-group mr-2">
                                <a class="btn btn-outline-danger btn-block" href="{{ url('/') }}">
                                    Regresar
                                </a>
                            </div>
                            <div class="btn-group mr-2">
                                <button type="submit" class="btn btn-outline-primary btn-block">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
