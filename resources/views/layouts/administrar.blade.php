@extends('welcome')
@section('principal')
    @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session('statusDelete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('statusDelete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session('statusUpdate'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('statusUpdate') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <div class="row m-5">
        <div class="col">
            <h1 class="mt-3 text-center">ADMINISTAR REGISTROS EN CIDENET</h1>
            <hr>
            <div class="row">
                <div class="col">
                    <form form class="was-validated" action="{{ url('/buscar') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col">
                                <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar"
                                    name="buscar" id="buscar" required>
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-outline-primary">
                                    Buscar
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search"
                                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
                                        <path fill-rule="evenodd"
                                            d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
                                    </svg>
                                </button>
                            </div>
                            @if ($bandera == true)
                                <div class="col">
                                    <a type="submit" class="btn btn-outline-danger" href="{{ url('/quitar') }}">
                                        Quitar
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                        </svg>
                                    </a>
                                </div>
                            @endif

                        </div>
                    </form>
                </div>

                <div class="col">
                    <h3 class="text-center">Agregar persona</h3>
                </div>
                <div class="col">
                    <a class="btn btn-outline-primary" href="{{ url('/agregar') }}">
                        Nuevo
                        <svg class="bi bi-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                            <path fill-rule="evenodd"
                                d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                        </svg>
                    </a>
                </div>

            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <table class="table text-center">

                        <thead class="table-dark">
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Tipo de Identificación</th>
                                <th>Identificación</th>
                                <th>Email</th>
                                <th>Otros Nombres</th>
                                <th>Segundo Apellido</th>
                                <th>País</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($personas as $persona)
                                <td>{{ $persona->primerNombre }}</td>
                                <td>{{ $persona->primerApellido }}</td>
                                <td>{{ $persona->tipoIdentificacion }}</td>
                                <td>{{ $persona->numeroIdentificacion }}</td>
                                <td>{{ $persona->correo }}</td>
                                <td>{{ $persona->otrosNombres }}</td>
                                <td>{{ $persona->segundoApellido }}</td>
                                <td>{{ $persona->pais }}</td>
                                <td>
                                    <!-- Botones acciones -->
                                    <div class="btn-group mr-2">
                                        <a class="btn btn-outline-primary" href="{{ url('/editar', $persona->id) }}">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                                <path fill-rule="evenodd"
                                                    d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="btn-group mr-2">
                                        <button class="btn btn-outline-danger" data-bs-toggle="modal"
                                            data-bs-target="#exampleModal" onclick="eliminar({{ $persona->id }})">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                    d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                            </svg>
                                        </button>
                                    </div>

                                </td>
                        </tbody>
                        <!-- Modal -->
                        <form class="was-validated" action="{{ url('/eliminar') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Eliminar persona</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <input type="hidden" name="id" id="id" class="form-control" name="id"
                                                    aria-describedby="idHelp" required>
                                                <small id="nombreHelp" class="form-text text-muted">¿Está seguro
                                                    de eliminar la persona?
                                                </small>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-danger">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function eliminar(id) {
            var modal = document.getElementById('exampleModal')
            var ide = document.getElementById('id')
            ide.value = id
        }

    </script>
@endsection
